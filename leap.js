const readline = require("readline-sync");

const answer = readline.question("Give an year to check if it's a leap year? ");

if (answer % 4 !== 0) console.log('Common year.');
else if (answer % 100 !== 0) console.log('Leap year.');
else if (answer % 400 !== 0) console.log('Common year.');
else console.log('Leap year.');
